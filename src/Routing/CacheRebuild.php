<?php

namespace Drupal\entity_cache_rebuild\Routing;

use Drupal\Core\Entity\ContentEntityType;
use Symfony\Component\Routing\Route;

class CacheRebuild {

  public function routes() {
    $routes = [];

    $entity_type_definitions = \Drupal::entityTypeManager()->getDefinitions();
    foreach ($entity_type_definitions as $definition) {
      if (!is_a($definition, ContentEntityType::class)) {
        continue;
      }
      $entity_type_id = $definition->id();
      if (($canonical = $definition->getLinkTemplate('canonical'))) {
        $route = new Route(
          // Path to attach this route to:
          "$canonical/cache-rebuild",
          // Route defaults:
          [
            '_controller' => '\Drupal\entity_cache_rebuild\Controller\CacheRebuild::rebuild',
            '_title' => 'Cache rebuild'
          ],
          // Route requirements:
          [
            '_permission'  => 'rebuild cache for all content entity types',
          ]
        );
        $route->addOptions(['entity_type_id' => $entity_type_id]);
        $routes["entity.$entity_type_id.cache_rebuild"] = $route;
      }
    }

    return $routes;
  }

}
