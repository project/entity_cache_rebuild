<?php

namespace Drupal\entity_cache_rebuild\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class CacheRebuildLocalTasks extends DeriverBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];

    $entity_type_definitions = \Drupal::entityTypeManager()->getDefinitions();
    foreach ($entity_type_definitions as $definition) {
      if (!is_a($definition, ContentEntityType::class)) {
        continue;
      }
      $entity_type_id = $definition->id();

      if (($definition->getLinkTemplate('canonical'))) {
        $this->derivatives['entity.' . $entity_type_id . '.cache_rebuild'] = [
          'route_name' => 'entity.' . $entity_type_id . '.cache_rebuild',
          'title' => $this->t('Cache rebuild'),
          'base_route' => 'entity.' . $entity_type_id . '.canonical',
        ];
      }
    }

    foreach ($this->derivatives as &$entry) {
      $entry += $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
