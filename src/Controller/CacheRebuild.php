<?php

namespace Drupal\entity_cache_rebuild\Controller;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class CacheRebuild extends ControllerBase {

  private KillSwitch $killSwitch;

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('page_cache_kill_switch'),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('messenger'),
    );
  }

  public function __construct(KillSwitch $kill_switch, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, MessengerInterface $messenger) {
    $this->killSwitch = $kill_switch;
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->messenger = $messenger;
  }

  public function rebuild(RouteMatchInterface $route_match) {
    $this->killSwitch->trigger();
    $route = $route_match->getRouteObject();

    $entity_type_id = $route->getOption('entity_type_id');
    $entity_id = $route_match->getParameter($entity_type_id);

    $entity = $this->entityTypeManager->getStorage($entity_type_id)
      ->load($entity_id);
    $cache_tags = $entity->getCacheTags();
    $message = "Cache rebuilt for $entity_type_id entity $entity_id.";

    $this->moduleHandler->alter('entity_cache_rebuild', $cache_tags, $message, $entity);

    Cache::invalidateTags($cache_tags);
    $this->messenger->addMessage($message);

    $route_name = "entity.$entity_type_id.canonical";
    $route_parameters = [$entity_type_id => $entity_id];
    $url = Url::fromRoute($route_name, $route_parameters)->toString();
    return new RedirectResponse($url);
  }

}
