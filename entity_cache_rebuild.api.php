<?php

/**
 * @file
 * Hooks related to Entity Cache Rebuild module.
 */

/**
 * @addtogroup hooks
 * @{
 */

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Alter the information provided in \Drupal\entity_browser\Annotation\EntityBrowserWidgetValidation.
 *
 * @param array $cache_tags
 *   An array with the cache tags that will be invalidated. It defaults to the
 *   entity's cache tags, that is returned by
 *   CacheableDependencyInterface::getCacheTags().
 *
 * @param $message
 *   The message that will be shown to the user.
 *
 * @param ContentEntityInterface $entity
 *   The entity whose cache tags are being invalidated.
 */
function hook_entity_cache_rebuild_alter(array &$cache_tags, &$message, ContentEntityInterface $entity) {
  // Example usage: When you want to also invalidate cache tags of an entity
  // that refers this one.
  $referring_entity_cache_tags = some_function_that_gets_referring_entities_cache_tags($entity);
  $cache_tags = array_merge($cache_tags, $referring_entity_cache_tags);
  $message = t("The cache of {$entity->label()} and all entities that refer it have been invalidated.");
}

/**
 * @} End of "addtogroup hooks".
 */
